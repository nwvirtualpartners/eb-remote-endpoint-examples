# EventBuilder Example Remote Endpoints

Example applications to use as remote endpoints for EventBuilder security

## Examples
* local-file: CSV backend
* query-mssql: Microsoft SQL backend
* query-mysql: MySQL backend

# How to use example

# Git clone
```
git clone https://bitbucket.org/nwvirtualpartners/eb-remote-endpoint-examples.git
cd eb-remote-endpoint-examples/
```

## Install NPM modules
```
$ npm install
```

## Run server example
```
$ npm run-script start-[local|mssql|mysql]
```