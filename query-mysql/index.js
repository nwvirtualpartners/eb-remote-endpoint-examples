const express = require('express');
const mysql = require('promise-mysql');
const app = express();

app.get('/', (req, res) => {
    if (!req.query.event) { return res.status(400).end(); }
    if (!req.query.value) { return res.status(400).end(); }

    mysql.createConnection({
        host: 'localhost',
        user: 'root',
        password: '',
        database: 'mydb'
    }).then(conn => {
        var result = conn.query('select * from `acl` where `event` = ? and `email` = ?', [ req.query.event, req.query.value ]);
        conn.end();
        return result;
    }).then(rows => {
        if (rows.length == 0) {
            return res.status(404).end(JSON.stringify({success: false}));
        } else {
            return res.status(200).end(JSON.stringify({success: true}));
        }
    });
});

app.listen(3000, () => {
    console.log(`EventBuilder remote endpoint listening on *:3000!`);
});