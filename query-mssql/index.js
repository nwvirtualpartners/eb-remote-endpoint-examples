const express = require('express');
const sql = require('mssql');
const app = express();

app.get('/', (req, res) => {
    if (!req.query.event) { return res.status(400).end(); }
    if (!req.query.value) { return res.status(400).end(); }

    sql.connect({
        server: 'localhost',
        user: 'root',
        password: '',
        database: 'mydb'
    }).then(() => {
        return new sql.Request()
        .input('event', sql.NVarChar, req.query.event)
        .input('email', sql.NVarChar, req.query.value)
        .query('select * from acl where event = @event and email = @email');
    }).then(result => {
        sql.close();
        if (result.recordset.length == 0) {
            return res.status(404).end(JSON.stringify({success: false}));
        } else {
            return res.status(200).end(JSON.stringify({success: true}));
        }
    }).catch(err => {
        console.error(err);
        return res.status(500).end();
    });
});

app.listen(3000, () => {
    console.log(`EventBuilder remote endpoint listening on *:3000!`);
});