const express = require('express');
const parse = require('csv-parse');
const fs = require('fs');
const app = express();
const list = [];

app.get('/', (req, res) => {
    if (!req.query.event) { return res.status(400).end(); }
    if (!req.query.value) { return res.status(400).end(); }

    if (list.find(item => item.event == req.query.event && item.email == req.query.value) == undefined) {
        return res.status(404).end(JSON.stringify({success: false}));
    } else {
        return res.status(200).end(JSON.stringify({success: true}));
    }
});

app.listen(3000, () => {
    console.log(`EventBuilder remote endpoint listening on *:3000!`);
    let parser = parse(fs.readFileSync('./example.csv'), {
        columns: true,
        delimiter: ',',
        skip_empty_lines: true,
        trim: true
    }).on('readable', () => {
        let record;
        while (record = parser.read()) { list.push(record); }
    }).on('error', function(err){
        console.error(err.message)
    }).on('end', () => {
        console.log(`Loaded ${list.length} records`);
    });
});